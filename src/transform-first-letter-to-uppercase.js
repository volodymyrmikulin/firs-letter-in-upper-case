const transformFirstLetterToUppercase = (str) => {
  if (typeof (str) !== 'string') {
    return 'Passed argument is not a string';
  }

  return str.split(" ").reduce((string, word) => {
    return `${string} ${word[0].toUpperCase() + word.slice(1)}`.trim();
  }, '');

};

module.exports = transformFirstLetterToUppercase;
